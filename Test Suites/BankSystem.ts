<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>BankSystem</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8721cdbf-7c39-4714-9167-d548c8099417</testSuiteGuid>
   <testCaseLink>
      <guid>cc892bfc-65ba-49a7-80fa-e7960eb325bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bank System/1 Login Bank System</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32ca0b0d-1b7a-4a9d-9a95-cb54cfb28d52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Bank System/2 Create Bank Account</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
