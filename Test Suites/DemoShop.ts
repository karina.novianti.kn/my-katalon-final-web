<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>DemoShop</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7bd4ce1c-dd86-4a0c-b701-e0273abbeae7</testSuiteGuid>
   <testCaseLink>
      <guid>25d2243c-9815-4804-96f8-d58d0c1ae93f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DemoShop/1 Open and navigate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9124a627-d658-4867-ac76-1e01a314b80f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DemoShop/2 DemoShop Registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65fe6208-c3eb-4a51-bbcf-a87a0f9053af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DemoShop/3 Demoshop Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54b56315-cebc-4c5e-a6ee-234749d5802d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DemoShop/4 Buy camera</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
